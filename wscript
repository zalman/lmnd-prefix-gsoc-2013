#!/usr/bin/env python
# encoding: utf-8
#
# this script is not executable by itself, but editors recognize the shebang
# and turn on python mode


# portage version used during bootstrap
TESTING_PV='2.2.01.21580.2'

import waflib
from waflib.Task import Task
# use waflib method to replace variables in strings for compatibility
from waflib.Utils import subst_vars as waf_subst_vars
import os, os.path, re, subprocess
import sys
import string
import shlex
from waflib import Options, Logs

#top = 'dist'
out = os.path.join('dist','waf_build')
APPNAME = 'lmonade'
#VERSION = ''

# supported targets
flavors = None
def _get_flavors():
    """
    Return a dictionary with supported flavors and corresponding
    build functions.
    """
    global flavors
    if flavors is None:
        flavors = {'bootstrap': (lambda x: 0),
        'plain': (lambda x: 0),
        'macaulay2': _build_generic,
        'pari': _build_generic,
        'polybori': _build_generic,
        'polymake': _build_generic,
        'sage': _build_sage,
        'singular': _build_generic}
    return flavors

def options(ctx):
    ctx.load('compiler_c compiler_cxx compiler_fc python perl')

    # waf alredy defines some command line options
    # remove some of these definitions to avoid confusion
    rm_grps = set()
    rm_grps.add(ctx.parser.get_option_group('-f')) # install/uninstall options
    rm_grps.add(ctx.parser.get_option_group('-o')) # configure options
    rm_grps.add(ctx.parser.get_option_group('-p')) # build and install options
    rm_grps.add(ctx.parser.get_option_group('--files')) # step options
    for gr in rm_grps:
        ctx.parser.option_groups.remove(gr)

    ctx.parser.add_option('-l', '--load-average', action='store', default=None,
            metavar='LOAD', dest='max_load',
            help='no new jobs are started if load average is above the given value. This option is passed on to MAKEOPTS configuration variable for portage.')

    lmnd = ctx.add_option_group('lmonade options')
    flavors_str = ', '.join(_get_flavors().keys())
    lmnd.add_option('-f','--flavor','--flavour', action='store', default='sage',
            help='specify which flavor to build, supported flavors are: '
            + flavors_str +
            ' (default sage)')
    lmnd.add_option('--force-flavor','--force-flavour', action='store', default=None,
            help='install a flavor not recognized by the --flavor option')

    lmnd_adv = ctx.add_option_group('lmonade advanced options')
    lmnd_adv.add_option('--bootstrap-portage-version',
            '--bootstrap-portage-version', action='store', default=TESTING_PV,
            dest='portage_ver', help='portage version used during bootstrap')

def configure(ctx):
    _check_if_in_prefix(ctx)
    # process options
    if not _get_flavors().has_key(ctx.options.flavor):
        raise ctx.errors.ConfigurationError(subst_vars(
            'flavor ${f} unknown, use --force-flavor to insist',
            dict(f=ctx.options.flavor)))
    ctx.env.flavor = ctx.options.force_flavor or ctx.options.flavor
    ctx.msg("Chosen flavor", ctx.env.flavor)

    ctx.msg("Number of jobs", ctx.options.jobs)

    # bootstrap portage version
    ctx.env.portage_ver = ctx.options.portage_ver
    ctx.msg("Portage version", ctx.options.portage_ver)

    ctx.env.missing = []
    _check_bash_version(ctx, (3,2))
    _find_gnu_prog(ctx, 'sed')      # check sed
    _find_gnu_prog(ctx, 'gawk')     # check gawk
    _find_gnu_prog(ctx, 'install')  # check coreutils
    _find_gnu_prog(ctx, 'find')     # check findutils
    _find_gnu_prog(ctx, 'grep')

    _find_prog(ctx, 'wget')

    ctx.find_program('less', var='LESS_BIN')
    _find_prog(ctx, 'yacc')

    _find_lib_pkgconfig(ctx, 'zlib')

    _check_compilers(ctx)

    _check_perl(ctx)

    _check_python(ctx, (2,7), (3,0))

    ctx.env.lmnd_base = ctx.path.abspath()
    ctx.env.prefix_base = os.path.join(ctx.path.abspath(), 'local')
    ctx.env.bootstrap_script = os.path.join(ctx.path.abspath(), 'dist',
            'portage', 'scripts', 'bootstrap-sage.sh')

    _check_make_env(ctx)


def build(ctx):
    _check_if_in_prefix(ctx)
    _check_make_env(ctx)
    _check_makeflags(ctx)
    ctx.to_log(subst_vars('Number of jobs: ${jobs}\n',
        dict(jobs=ctx.options.jobs)))
    ctx.to_log(subst_vars('Maximum load: ${load}\n',
        dict(load=ctx.options.max_load)))
    os.environ['PATH']=':'.join([os.path.join(ctx.env.prefix_base, 'bin'),
            os.path.join(ctx.env.prefix_base, 'tmp', 'local', 'bin'),
            os.path.join(ctx.env.prefix_base, 'tmp', 'local', 'usr', 'bin'),
            os.environ['PATH']])
    os.environ['EPREFIX']=ctx.env.prefix_base
    os.environ['LMNDBASE']=ctx.env.lmnd_base
    os.environ['LMND_LOCAL']=os.path.join(ctx.env.lmnd_base, 'local')
    # portage version used for bootstrap
    os.environ['TESTING_PV']=ctx.env.portage_ver

    wget = BootstrapOrLink('wget', env=ctx.env)
    ctx.add_to_group(wget)

    yacc = BootstrapOrLink('yacc', package_name='bison', tmp=True, env=ctx.env)
    ctx.add_to_group(yacc)

    bash = BootstrapOrLink('bash',env=ctx.env, run_after=[yacc])
    ctx.add_to_group(bash)

    sed = BootstrapOrLink('sed',env=ctx.env)
    ctx.add_to_group(sed)

    grep = BootstrapOrLink('grep', env=ctx.env)
    ctx.add_to_group(grep)

    coreutils = BootstrapOrLink('install', package_name='coreutils',
            env=ctx.env)
    ctx.add_to_group(coreutils)

    findutils = BootstrapOrLink('find', package_name='findutils', env=ctx.env)
    ctx.add_to_group(findutils)

    gawk = BootstrapOrLink('gawk',env=ctx.env)
    ctx.add_to_group(gawk)

    if 'python' in ctx.env.missing:
        zlib = BootstrapIfMissing('zlib', tmp=True, env=ctx.env)
        ctx.add_to_group(zlib)

    # Bootstrap python to a temporary directory. Otherwise, when it gets
    # pulled as a dependency of another package emerge fails because of
    # file collisions.
    python = BootstrapOrLink('python', tmp=True, env=ctx.env)
    ctx.add_to_group(python)

    ctx.add_group() # so that the previous steps are completed before going on

    portage = BootstrapOrLink('portage', force=True, env=ctx.env,
            run_after=[python, bash, gawk, sed, wget])
    ctx.add_to_group(portage)

    # form a string that is used in the MAKEOPTS variable in portage's make.conf
    make_opts_args = ['-j', str(ctx.options.jobs)]
    if ctx.options.max_load:
        make_opts_args.extend(('-l', str(ctx.options.max_load)))
    make_opts_str = ' '.join(make_opts_args)

    # write simple make.conf
    simple_make_conf_str = subst_vars('''
USE="-* readline zlib jpeg png"
MAKEOPTS="${make_opts_str} VERBOSE=1"
FEATURES="nostrip lmirror"
PYTHON_TARGETS="python2_7"
''', dict(make_opts_str=make_opts_str))

    simple_make_conf = WriteFile(\
            os.path.join(ctx.env.prefix_base,'etc', 'portage', 'make.conf'),
            simple_make_conf_str, env=ctx.env)
    ctx.add_to_group(simple_make_conf)

    # create directories and files under $EPREFIX/local/etc/portage
    # used for portage configuration

    # package.keywords directory
    ctx(name='create_package_keywords',
        rule=subst_vars('mkdir -p ${tgt}',
            dict(tgt=os.path.join(ctx.env.prefix_base,
            'etc','portage','package.keywords'))))

    # an empty file in package.keywords/
    # this is needed for emerge --autounmask
    ctx(rule=subst_vars('touch ${tgt}',
        dict(tgt=os.path.join(ctx.env.prefix_base,
            'etc', 'portage', 'package.keywords', 'auto'))),
        after='create_package_keywords')

    # package.use directory
    ctx(name='create_package_use',
        rule=subst_vars('mkdir -p ${tgt}',
            dict(tgt=os.path.join(ctx.env.prefix_base,
            'etc','portage','package.use'))))

    # an empty file in package.use/
    # this is needed for emerge --autounmask
    ctx(rule=subst_vars('touch ${tgt}',
        dict(tgt=os.path.join(ctx.env.prefix_base,
            'etc', 'portage', 'package.use', 'auto'))),
        after='create_package_use')

    # link to profiles/lmnd/package.use from the profiles dir
    ctx(rule=subst_vars(
        'ln -sf ../../../../dist/portage/profiles/lmnd/package.use ${tgt}',
        dict(tgt=os.path.join(ctx.env.prefix_base, 'etc','portage',
            'package.use', 'profile'))),
        after='create_package_use')

    # package.env directory
    # used to set environment variables while building specific packages
    ctx(name='create_portage_env',
        rule=subst_vars('mkdir -p ${tgt}',
            dict(tgt=os.path.join(ctx.env.prefix_base,
            'etc','portage','env'))))

    # symlink devel.env in the portage tree to users package.env directory
    # devel.env contains environment variables needed to set up a
    # development environment for certain packages using live ebuilds
    ctx(rule=subst_vars(
        'ln -sf ../../../../dist/portage/scripts/devel.env ${tgt}',
        dict(tgt=os.path.join(ctx.env.prefix_base, 'etc','portage','env',
            'devel.env'))),
        after='create_portage_env')

    if not ctx.env.PERL.startswith(ctx.env.prefix_base):
        ctx(rule=subst_vars('ln -sf ${src} ${tgt}', dict(src=ctx.env.PERL,
                tgt=os.path.join(ctx.env.prefix_base,'bin',''))))
    if not ctx.env.LESS_BIN.startswith(ctx.env.prefix_base):
        ctx(rule=subst_vars('ln -sf ${src} ${tgt}', dict(src=ctx.env.LESS_BIN,
                tgt=os.path.join(ctx.env.prefix_base,'bin',''))))
    ctx(rule=subst_vars('ln -sf ${src} ${tgt}', dict(
            src=os.path.join(ctx.env.prefix_base,'bin','bash'),
            tgt=os.path.join(ctx.env.prefix_base,'bin','sh'))))

    ctx.add_group() # so that the previous steps are completed before going on

    compilerwrapper = EmergePackage('compilerwrapper', env=ctx.env)
    ctx.add_to_group(compilerwrapper)

    autoconf = EmergePackage('autoconf', env=ctx.env, collision_protect=False,
            run_after=[compilerwrapper])
    ctx.add_to_group(autoconf)

    automake = EmergePackage('automake', env=ctx.env, collision_protect=False,
            run_after=[compilerwrapper])
    ctx.add_to_group(automake)

    rportage = EmergePackage('portage', deep=True, collision_protect=False,
            run_after=[autoconf, automake], env=ctx.env)
    ctx.add_to_group(rportage)

    real_make_conf_str = subst_vars(r'''
USE=" data -gdbm gmp jpeg libpolymake ntl -openmp png python \
        readline sage sqlite ssl zlib"
MAKEOPTS="${make_opts_str} VERBOSE=1"
FEATURES="nostrip lmirror"
PYTHON_TARGETS="python2_7"
''', dict(make_opts_str=make_opts_str))

    real_make_conf = WriteFile(\
            os.path.join(ctx.env.prefix_base, 'etc', 'portage', 'make.conf'),
            real_make_conf_str, env=ctx.env, run_after=[rportage])
    ctx.add_to_group(real_make_conf)

    ctx.add_group() # complete previous steps before going on

    # Clean temporary directory to avoid collisions in case anything was
    # bootstrapped there. This is in a separate group since there is no way
    # to indicate it should be executed after emerging portage.
    ctx(rule=subst_vars('rm -rf ${tmpdir}',
        dict(tmpdir=os.path.join(ctx.env.prefix_base,'tmp','local'))))

    # bootstrap script can leave temporary files in lmnd_base/tmp
    ctx(rule=subst_vars('rm -rf ${tmpdir}',
        dict(tmpdir=os.path.join(ctx.env.lmnd_base,'tmp'))))

    ctx.add_group() # complete previous steps before going on

    baselayout = EmergePackage('baselayout-prefix', env=ctx.env)
    ctx.add_to_group(baselayout)

    startscript = BootstrapOrLink('startscript', force=True, env=ctx.env)
    ctx.add_to_group(startscript)

    ctx.add_group() # so that the previous steps are completed before going on

    flavors[ctx.env.flavor](ctx)


def _build_sage(ctx):
    sage = EmergePackage('sage', new_use=True, env=ctx.env)
    ctx.add_to_group(sage)


def _build_generic(ctx):
    tgt = EmergePackage(ctx.env.flavor, new_use=True, env=ctx.env)
    ctx.add_to_group(tgt)

# the default distclean() function removes the build directory and lockfiles
#def distclean(ctx):
#    print(' Not cleaning anything!')

def dist(ctx):
    # I have no clue what this does by default, but it should be equivalent
    # to sage -bdist at some point
    pass


##########################################################################
# Utility functions
#
# function names should start with _
# otherwise the function will be listed in "waf --help" output
##########################################################################

def _find_prog(ctx, prog):
    """
    Search path for given command.

    Sets variable in environment if the command is found. Otherwise the command
    is appended to list the list of missing packages which will be bootstrapped.
    """
    try:
        ctx.find_program(prog, var=prog.upper())
    except ctx.errors.ConfigurationError:
        ctx.to_log(subst_vars('${pname} was not found', dict(pname=prog)))
        ctx.env.missing.append(prog)
        return False
    return True

def _find_gnu_prog(ctx, prog):
    """
    Check if given command is the GNU version.
    """
    if not _find_prog(ctx, prog):
        # program doesn't exist no need to check for a GNU version
        return

    try:
        out = ctx.cmd_and_log([getattr(ctx.env,prog.upper()), '--version'],
            output=waflib.Context.STDOUT, quiet=waflib.Context.BOTH)
    except ctx.errors.WafError:
        ctx.env.missing.append(prog)
        return

    if out.find('GNU') == -1:
        ctx.to_log(subst_vars('${pname} is not GNU', dict(pname=prog)))
        ctx.env.missing.append(prog)

def _find_lib_pkgconfig(ctx, libname):
    """
    Using pkg-config configuration files, check if a library is available
    on the host. This script reads pkgconfig files directly so that it works
    even if there is no pkg-config executable on the host system.
    """
    msg_str = "Checking for library %s"%libname
    try:
        pkgconfig = PkgConfig(libname)
    except ValueError:
        ctx.env.missing.append(libname)
        ctx.msg(msg_str, "not found")
        return False
    # FIXME: handle non standard library/header paths
    ctx.msg(msg_str, "found")
    return True

def _check_bash_version(ctx, version):
    """
    Check if bash is available with minimum version.

    version -- tuple of 2 integers, the major and minor version numbers.
    """
    if not _find_prog(ctx, 'bash'):
        # bash is not available no need to check the version
        return

    out = ctx.cmd_and_log("echo $BASH_VERSION", executable=ctx.env.BASH,
            output=waflib.Context.STDOUT)#, quiet=waflib.Context.BOTH)
    bver = tuple(map(int, out.split('.')[:2]))
    if bver > version:
        return
    ctx.to_log(subst_vars('base version ${ver} is too old', dict(ver=out)))
    ctx.env.missing.append('bash')

# check compiler versions
def _check_compilers(ctx):
    """
    Check if c, c++ and fortran compilers are available and they all have
    the same version.
    """
    ctx.load('compiler_c')
    ctx.get_cc_version(ctx.env.CC)
    cc_version = ctx.env.CC_VERSION

    ctx.load('compiler_cxx')
    ctx.get_cc_version(ctx.env.CXX)
    cxx_version = ctx.env.CC_VERSION

    try:
        ctx.load('compiler_fc')
        ctx.find_gfortran()
    except ctx.errors.ConfigurationError:
        ctx.to_log('gfortran was not found')
        ctx.env.missing.append('gcc')
    else:
        ctx.get_gfortran_version([ctx.env.FC])
        fc_version = ctx.env.FC_VERSION

    if cc_version != cxx_version and \
            sys.platform.startswith('linux') and cc_version != fc_version:
        ctx.fatal("compiler versions do not match")

    ctx.find_ar()

def _check_perl(ctx):
    """
    Check if perl is available and we can build extensions.
    """
    ctx.load('perl')
    ctx.check_perl_version()
    ctx.check_perl_ext_devel()

def _check_python(ctx, min_version, max_version):
    """
    Check if system has a python install with the given minimum version.

    Besides the check_python() function provided by waf, we look through
    the directories in the path and try other python executables.

    version -- tuple of ints representing minimum required python version
    """
    ctx.load('python')
    min_failed = False
    max_ver_str = '.'.join(map(str, max_version))
    def check_max_version():
        if ctx.env.PYTHON_VERSION > max_ver_str:
            return False
        return True

    try:
        ctx.check_python_version(min_version)
    except ctx.errors.ConfigurationError:
        min_failed = True

    if min_failed or not check_max_version():

        # system python (/usr/bin/python) is too old
        # search the path for other python binaries
        import glob
        dirs = os.environ['PATH'].split(':')
        for d in dirs:
            executables = glob.iglob(os.path.join(d,'python[0-9].[0-9]'))
            for e in executables:
                ctx.to_log("trying python executable at " + str(e) +'\n')
                ctx.env['PYTHON'] = [e]
                min_failed=False
                try:
                    ctx.check_python_version(min_version)
                except ctx.errors.ConfigurationError:
                    min_failed = True

                if min_failed or not check_max_version():
                    continue

                break
            else:
                continue
            break
        else:
            ctx.to_log("Cannot find suitable python version, adding it to bootstrap  list\n")
            ctx.env.missing.append('python')
            return

    ctx.msg("Found suitable python:",ctx.env.PYTHON[0])
    ctx.env.PYTHON=ctx.env.PYTHON[0] # check_python_version returns a list

def _check_if_in_prefix(ctx):
    lmndbase = os.environ.get('LMNDBASE')
    if lmndbase is not None:
        ctx.fatal("LMNDBASE environment variable defined.\n"
                "It is very likely that you are running make within a prefix environment.\n"
                "Please try again after exiting the prefix shell (Ctrl-D).")


def _check_make_env(ctx):
    make_env = os.environ.get('MAKE')
    if make_env is not None and \
            ('jobs' in make_env or '-j' in make_env):
        Logs.error("MAKE environment variable contains --jobs or -j parameter.")
        Logs.error("This may cause build failures in certain packages.")
        ctx.fatal("Please use the MAKEFLAGS environment variable to pass arguments to make." )


regexp_from_type = {int: '^(\s|=)?[0-9]*', float:'^(\s|=)?[0-9]*(\.[0-9]+)?' }
def _parse_string_for_value(ctx, string, keywords, restype):
    """
    Given a string argument, extract the value assigned to keyword and
    convert it to restype.

    This is used to parse environment variables that should effect other
    programs to set number of parallel processes we can use.
    """
    ind = -1
    for key in keywords:
        ind = string.find(key)
        if ind != -1:
            # if keyword was found
            # set ind to point to the end of the keyword
            ind += len(key)
            break
    else:
        # if no keyword was found in the given string, return None
        return None

    # we allow -option=<value>, -option value or -o<value>
    # in case an equal sign is used, skip that before parsing
    if string[ind] == '=':
        ind += 1

    # extract the integer following the argument
    itr = re.finditer(regexp_from_type[restype], string[ind:])
    try: # in case there were no matches
        match = itr.next()
    except StopIteration:
        return None

    try: # in the off chance that conversion does not work
        res = restype(match.group())
    except ValueError:
        return None

# inspired by the corresponding function from the Samba build system
# git.samba.org/?p=tridge/samba.git;a=blob;f=buildtools/wafsamba/samba_utils.py
def _check_makeflags(bld):
    """
    Check for MAKEFLAGS environment variable to try to honor a few make
    command line flags.

    In case we are being called from a Makefile, use the jobserver created
    by make to determine how many parallel processes we can run.
    """
    # the convenience Makefile generated by the configure script
    # defines WAF_MAKE=1 before running waf
    makeflags = os.environ.get('MAKEFLAGS')
    if makeflags is None:
        return
    if not 'WAF_MAKE' in os.environ:
        # we are not being called from a makefile
        # so we don't need to work with an existing make process

        # check if the environment variable contains any arguments that
        # might be interesting for us
        bld.options.jobs = _parse_string_for_value(bld, makeflags,
                ('-j', '--jobs'), int)
        bld.options.max_load = _parse_string_for_value(bld, makeflags,
                ('-l', '--load-average'), float)
        return

    jobs_set = False
    # we need to use shlex.split to cope with the escaping of spaces
    # in makeflags
    jobserver_fd = 0
    for opt in shlex.split(makeflags):
        # options can come either as -x or as x
        if opt[0] != '-':
            for v in opt:
                if v == 'j':
                    jobs_set = True
                elif v == 'k':
                    Options.options.keep = True
        elif opt == '-j':
            jobs_set = True
        elif opt == '-k':
            Options.options.keep = True
        elif opt.startswith('--jobserver-fds='):
            sep = opt.find(',')
            # 16 = len('--jobserver-fds=')
            jobserver_fd = int(opt[16:sep])
            # sep + 1 is the index of the string with the id of the second
            # file descriptor
            bld.env.jobserver_write_fd = int(opt[sep+1:])

    if not jobs_set:
        # default to one job
        Options.options.jobs = 1
    else:
        njobs = _n_jobs_from_jobserver(bld, jobserver_fd)
        Options.options.jobs = njobs

    match = re.match('^l (?P<max_load>[0-9]*(\.[0-9]+)?)', makeflags)
    if match is not None:
        load_str = match.group('max_load')
    else:
        return
    try:
        bld.options.max_load = float(load_str)
    except ValueError:
        pass


def _n_jobs_from_jobserver(bld, fd):
    """
    Use make's jobserver to decide number of parallel processes we can use.
    """
    # set the non blocking flag. This is only available on UNIX and OSX
    import fcntl
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

    # The jobserver is simply a file descriptor. The protocol is to read
    # one byte for each process and put it back once the process is done.
    # We read single bytes in nonblocking mode as much as possible. The
    # function _restore_jobserver() restores these on exit.
    jobserver = os.fdopen(fd, 'rb')
    cur_pos = 0
    try:
        while jobserver.read(1):
            cur_pos += 1
    except IOError:
        pass
    bld.env.jobserver=cur_pos
    bld.add_post_fun(_restore_jobserver)
    return cur_pos+1

def _restore_jobserver(ctx):
    """
    Restore state of make jobserver to prevent make displaying an error
    message when we are done.
    """
    jobserver = os.fdopen(ctx.env.jobserver_write_fd, 'wb')
    jobserver.write(' '*ctx.env.jobserver)



def subst_vars(expr, params):
    """
    Replace occurences of ${varname} in expr with the value of varname in
    dictionary supplied in params.

    This is a wrapper around waflib.Utils.subst_vars to convert the items in
    params to string automatically, similar to the behavior of str.format() in
    later Python versions.
    """
    return waf_subst_vars(expr,
            dict(map(lambda x: (x[0],str(x[1])), params.items())))


class TaskCustomUid(Task):
    """
    The default implementation of uid() (unique id) in waflib uses the list of
    inputs and outputs of the task. Since the custom tasks below do not have
    these properties, all tasks with the same class get the same uid.
    This confuses waf when it tries to decide which tasks were already executed.

    This base class defines a function which generates a unique id and stores
    it in the cache used by waflib's uid(). This function should be called
    from the __init__ method of subclasses.
    """
    def __init__(self, *args, **kwds):
        super(TaskCustomUid, self).__init__(*args, **kwds)
        after_list = kwds.get('run_after', None)
        if after_list:
            self.run_after.update(after_list)

    def gen_uid(self, *args):
        """
        This function must return a value that will be consistent between
        different runs, so hash() is not an option.
        """
        res = waflib.Utils.md5()
        res.update(self.__class__.__name__.encode('utf-8'))
        for i in args:
            res.update(str(i).encode('utf-8'))
        self.uid_ = res.digest()

    def __repr__(self):
        return " ".join((self.__class__.__name__, self._name))

class BootstrapIfMissing(TaskCustomUid):
    """
    Call Gentoo Prefix bootstrap script to install a package if it was not
    found on the host in the configure phase.

    Missing packages are stored in the `missing` attribute of the current
    environment by various functions during configure.
    """
    def __init__(self, name, force=False, tmp=False, **kwds):
        """
        name -- name of the package
        force -- if True, call bootstrap even if command is not missing
        tmp -- if True bootstrap the package into EPREFIX/tmp
        """
        super(BootstrapIfMissing, self).__init__(**kwds)
        self.gen_uid(name, force, tmp)
        self._name = name
        self._force = force
        self._tmp = tmp

    def run(self):
        """
        Call bootstrap-prefix.sh to bootstrap `self._name` if it is not
        already installed on the host.
        """
        #FIXME: log output to file
        #FIXME: error message to self.last_cmd
        prefix_dir = self.env.lmnd_base + ('/dist/tmp' if self._tmp else '')
        if self._name in self.env.missing or self._force:
            return self.exec_command(
                    subst_vars('${bootstrap} ${eprefix} ${name}', dict(
                bootstrap=self.env.bootstrap_script,
                eprefix=prefix_dir,
                name=self._name)))
        return 0


class BootstrapOrLink(BootstrapIfMissing):
    """
    Either call the bootstrap script to install the given program or
    symlink the executable from the host. The list of missing packages
    compiled during the config phase is used to decide which.
    """
    def __init__(self, command, package_name=None, force=False, tmp=False,
            **kwds):
        """
        command -- name of the command this is also used to determine the
            variable name in the ConfigSet containing the full path to the
            command
        package_name -- name of package to install if command is missing on
            the host, same as command if not specified
        force -- if True, call bootstrap even if command is not missing
        tmp -- if True bootstrap the package into EPREFIX/tmp
        """
        super(BootstrapOrLink, self).__init__(command, force=force,
                tmp=tmp, **kwds)
        if package_name:
            self._pname = package_name
        else:
            self._pname = command

    def run(self):
        if self._name in self.env.missing or self._force:
            self._name = self._pname # BootstrapIfMissing uses self._name
            status = super(BootstrapOrLink, self).run()
        else:
            src = self.env[self._name.upper()]
            if src.startswith(self.env.prefix_base):
                # if target for the symlink is already in the prefix
                # do not create a circular link
                return 0
            status = self.exec_command(subst_vars('ln -sf ${src} ${dest}',
                dict(src=src, dest=os.path.join(self.env.prefix_base,
                    'bin', self._name))))
        return status


class EmergePackage(TaskCustomUid):
    def __init__(self, name, deep=False, collision_protect=True,
            new_use=False, **kwds):
        """
        name -- name of package to emerge
        deep -- pass -D to emerge, do not stop traversing the depency tree
            when a package not needing update is found
        collision_protect -- disable collision protection for installed files
        new_use -- pass --newuse to emerge, rebuild a package even if only
            a use flag changed
        """
        super(EmergePackage, self).__init__(**kwds)
        self.gen_uid(name, deep, collision_protect, new_use)
        self._name = name
        self._cprotect = collision_protect
        self._deep = deep
        self._new_use = new_use

    def run(self):
        if not self._cprotect:
            os.environ['FEATURES']="-collision-protect"
        #FIXME: log output to file
        ret = self.exec_command(
                subst_vars('${emerge} ${deep} ${new_use} ${tgt}',
            dict(emerge=os.path.join(self.env.prefix_base,'bin','emerge'),
            deep='-D' if self._deep else '',
            new_use='--newuse' if self._new_use else '',
            tgt=self._name)))
        #FIXME: error message to self.last_cmd
        if not self._cprotect:
            os.environ['FEATURES']=""
        return ret

class WriteFile(TaskCustomUid):
    """
    Write given data to the specified file
    """
    def __init__(self, filename, data, **kwds):
        super(WriteFile, self).__init__(**kwds)
        self.gen_uid(filename, data)
        self._name = filename
        self._data = data

    def run(self):
        dir_name = os.path.dirname(self._name)
        if not os.path.exists(dir_name):
            try:
                os.makedirs(dir_name)
            except OSError:
                exc = sys.exc_info()[1]
                import errno
                if not( exc.errno == errno.EEXIST and os.path.isdir(path) ):
                    raise
        outf = open(self._name, 'w')
        outf.write(self._data)
        outf.close()

# based on http://code.activestate.com/recipes/464406/
class PkgConfig(dict):
    """
    This class wraps metadata about installed libraries extracted from
    pkgconfig files on the host system.

    In order to allow this to work without installing pkg-config, a simple
    parser for pkgconfig files is included.
    """
    _paths = ['/usr/lib/pkgconfig', '/usr/share/pkgconfig',
            '/usr/local/lib/pkgconfig', '/usr/local/share/pkgconfig']

    def __init__(self, package):
        """
        Search standard paths for .pc files for metadata about `package`.

        Raises ValueError if no information is found.
        """
        # add contents of PKG_CONFIG_PATH to self._paths
        try:
            pkg_config_path = os.environ['PKG_CONFIG_PATH']
        except KeyError:
            pass
        else:
            self._paths.extend(pkg_config_path.split(':'))

        for path in self._paths:
            fn = os.path.join(path, '%s.pc' % package)
            if os.path.exists(fn):
                self._parse(fn)
                break
        else:
            raise ValueError("pkgconfig data not found for package %s" %
                    package)

    def _parse(self, filename):
        """
        Parse a pkgconfig .pc file, performing variable substitutions if
        necessary.
        """
        lines = open(filename).readlines()

        variables = {}

        for line in lines:
            line = line.strip()

            if not line:
                continue
            elif ':' in line: # exported variable
                name, val = line.split(':')
                val = val.strip()
                if '$' in val:
                    try:
                        val = string.Template(val).substitute(variables)
                    except ValueError:
                        raise ValueError("Error in variable substitution!")
                self[name] = val
            elif '=' in line: # local variable
                name, val = line.split('=')
                if '$' in val:
                    try:
                        val = string.Template(val).substitute(variables)
                    except ValueError:
                        raise ValueError("Error in variable substitution!")
                variables[name] = val
